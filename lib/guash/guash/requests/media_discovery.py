import logging
import os
import threading
from shutil import copy
from typing import Dict, List, Optional, TYPE_CHECKING

from guash.request import RequestId, ResponseId, GuashClientRequest, GuashServerResponse
from satnet.request import request, response, ResponseCallback

if TYPE_CHECKING:
    from guash.client.session import GuashLocalSession
    from guash.server.session import GuashRemoteSession

logger = logging.getLogger(__name__)


@request(id=RequestId.MEDIA_DISCOVERY)
class MediaDiscoveryRequest(GuashClientRequest):
    """
    Request a list of files from inside a folder on the splash server
    """
    _fields = ['directory_attribute']

    def __init__(self, dir_attribute: Optional[str] = None):
        super().__init__()
        self.directory_attribute = dir_attribute

    def handle(self, session: 'GuashRemoteSession', respond: ResponseCallback) -> None:
        if not self.directory_attribute:
            return

        folder = session.server.controller.get_object_attribute(session.server.controller.get_interpreter_name(), self.directory_attribute)
        if not folder or len(folder) != 1 or not isinstance(folder[0], str):
            logger.error('Invalid value for attribute {} of object python'.format(self.directory_attribute))
            return
        respond(MediaDiscoveryResponse(session.server.controller.get_media_list(folder=folder[0]), self.directory_attribute))


@response(id=ResponseId.MEDIA_DISCOVERY)
class MediaDiscoveryResponse(GuashServerResponse[MediaDiscoveryRequest]):
    """
    Answer a client request for a list of files in a given folder
    """
    _fields = ['file_list', 'directory']

    def __init__(self, file_list: Optional[List[Dict]] = None, directory: Optional[str] = None) -> None:
        super().__init__()
        self.file_list = file_list
        self.directory = directory

    def handle(self, request: MediaDiscoveryRequest, session: 'GuashLocalSession') -> None:
        assert isinstance(request, MediaDiscoveryRequest)
        session.client.app.guash_root.update_file_list(self.file_list, self.directory)
        logger.debug('Received response to file discovery request: {}'.format(str(self.file_list)))


@request(id=RequestId.ACTIVATE_MEDIA)
class ActivateMediaRequest(GuashClientRequest):
    """

    """
    _fields = ['filename']

    def __init__(self, filename: Optional[str] = None):
        super().__init__()
        self.filename = filename

    @staticmethod
    def must_copy(src: str, dest: str) -> bool:
        if not os.path.exists(dest) or os.stat(src).st_size != os.stat(dest).st_size:
            return True
        return False

    def copy(self, src: str, dest: str, respond: ResponseCallback) -> None:
        file_status = None
        try:
            if self.must_copy(src, dest):
                copy(src, dest)
        except IOError as e:
            logger.warning('Unable to copy file {}: {}'.format(src, e))
        else:
            file_status = self.filename

        respond(ActivateMediaResponse(filename=file_status))

    def handle(self, session: 'GuashRemoteSession', respond: ResponseCallback) -> None:
        src_folder = session.server.controller.get_object_attribute(session.server.controller.get_interpreter_name(), 'mediaDirectory')
        dest_folder = session.server.controller.get_object_attribute(session.server.controller.get_interpreter_name(), 'activeMediaDirectory')
        if not src_folder or len(src_folder) != 1 or not isinstance(src_folder[0], str) \
                or not dest_folder or len(dest_folder) != 1 or not isinstance(dest_folder[0], str):
            logger.error('Failed to fetch media directory')
        else:
            base_folder = os.path.join(dest_folder[0], os.path.dirname(self.filename))
            if not os.path.exists(base_folder):
                os.makedirs(base_folder)
            src_fullpath = os.path.join(src_folder[0], self.filename)
            dest_fullpath = os.path.join(dest_folder[0], self.filename)
            if not os.path.exists(src_fullpath):
                logger.error('Could not find source file {}'.format(src_fullpath))
            else:
                th = threading.Thread(target=self.copy,
                                      kwargs={'src': src_fullpath, 'dest': dest_fullpath, 'respond': respond})
                th.start()


@response(id=ResponseId.ACTIVATE_MEDIA)
class ActivateMediaResponse(GuashServerResponse[MediaDiscoveryRequest]):
    """

    """
    _fields = ['filename']

    def __init__(self, filename: Optional[str] = None) -> None:
        super().__init__()
        self.filename = filename

    def handle(self, request: MediaDiscoveryRequest, session: 'GuashLocalSession') -> None:
        assert isinstance(request, ActivateMediaRequest)
        session.client.app.guash_root.finished_activation(self.filename)
        logger.debug('Activation of media {} finished successfully'.format(self.filename))
