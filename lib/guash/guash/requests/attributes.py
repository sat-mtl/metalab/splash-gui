import logging
from typing import TYPE_CHECKING

from guash.request import RequestId, ResponseId, GuashClientRequest, GuashServerResponse
from satnet.request import request, response, ResponseCallback

if TYPE_CHECKING:
    from guash.client.session import GuashLocalSession
    from guash.server.session import GuashRemoteSession

logger = logging.getLogger(__name__)


@request(id=RequestId.GET_ATTRIBUTE)
class GetAttributeRequest(GuashClientRequest):
    """
    Request a splash sink buffer from the server
    """

    def __init__(self, object_name: str, attribute: str) -> None:
        super().__init__()
        self._object_name = object_name
        self._attribute = attribute
        self._value = ''

    @property
    def project_list(self):
        return self._project_list

    def handle(self, session: 'GuashRemoteSession', respond: ResponseCallback) -> None:
        self._value = session.server.controller.get_object_attribute(object_name=self._object_name,
                                                                     attribute=self._attribute)
        respond(GetAttributeResponse())


@response(id=ResponseId.GET_ATTRIBUTE)
class GetAttributeResponse(GuashServerResponse[GetAttributeRequest]):
    """
    Answer a client request for a splash sink buffer
    """

    def __init__(self) -> None:
        super().__init__()

    def handle(self, request: GetAttributeRequest, session: 'GuashLocalSession') -> None:
        assert isinstance(request, GetAttributeRequest)
        session.project_dir = request.project_list
        logger.debug(session.project_dir)
