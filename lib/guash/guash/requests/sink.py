import logging
from typing import TYPE_CHECKING

from guash.request import RequestId, ResponseId, GuashClientRequest, GuashServerResponse
from satnet.request import request, response, ResponseCallback

if TYPE_CHECKING:
    from guash.client.session import GuashLocalSession
    from guash.server.session import GuashRemoteSession

logger = logging.getLogger(__name__)


@request(id=RequestId.OPEN_SINK_BUFFER)
class OpenSinkRequest(GuashClientRequest):
    """
    Request a splash sink buffer from the server
    """
    _fields = ['object']

    def __init__(self, obj_name: str = None) -> None:
        super().__init__()
        self.object = obj_name

    def handle(self, session: 'GuashRemoteSession', respond: ResponseCallback) -> None:
        if session.server.controller.open_sink(self.object):
            session.server.notify_sink(self.object)
        respond(OpenSinkResponse())


@response(id=ResponseId.OPEN_SINK_BUFFER)
class OpenSinkResponse(GuashServerResponse[OpenSinkRequest]):
    """
    Answer a client request for a splash sink buffer
    """

    def __init__(self) -> None:
        super().__init__()

    def handle(self, request: OpenSinkRequest, session: 'GuashLocalSession') -> None:
        assert isinstance(request, OpenSinkRequest)


@request(id=RequestId.CLOSE_SINK_BUFFER)
class CloseSinkRequest(GuashClientRequest):
    """
    Request a splash sink buffer to be closed on the server
    """
    _fields = ['object']

    def __init__(self, obj_name: str = None) -> None:
        super().__init__()
        self.object = obj_name

    def handle(self, session: 'GuashRemoteSession', respond: ResponseCallback) -> None:
        session.server.stop_notify('sink')
        session.server.controller.close_sink(self.object)
        respond(CloseSinkResponse())


@response(id=ResponseId.CLOSE_SINK_BUFFER)
class CloseSinkResponse(GuashServerResponse[CloseSinkRequest]):
    """
    Request a splash sink buffer to be closed on the server
    """
    _fields = ['object']

    def __init__(self, obj_name: str = None) -> None:
        super().__init__()
        self.object = obj_name

    def handle(self, request: CloseSinkRequest, session: 'GuashLocalSession') -> None:
        session.client.app.guash_root.flush_render(self.object)

