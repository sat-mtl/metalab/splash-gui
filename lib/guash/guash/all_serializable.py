# Import all serializable classes, they should be imported even if not directly used in code because a serializable
# might be coming from the network and we should be able to deserialize and handle it.


# noinspection PyUnresolvedReferences
def import_all() -> None:
    # Actions

    # Commands
    from guash.commands.attributes import SetObjectAttribute, SetWorldAttribute
    from guash.commands.media import PreviewMedia
    from guash.commands.projects import LoadProject
    from guash.commands.subscriptions import SubscribeNotif, UnsubscribeNotif

    # Notifications
    from guash.notifications.perfs import MasterClock, GetFPS
    from guash.notifications.sink import SinkRender

    # Requests
    from guash.requests.attributes import GetAttributeRequest, GetAttributeResponse
    from guash.requests.media_discovery import MediaDiscoveryRequest, MediaDiscoveryResponse, ActivateMediaRequest, \
        ActivateMediaResponse
    from guash.requests.media_info import MediaInfoRequest, MediaInfoResponse
    from guash.requests.object_discovery import ObjectsOfTypeRequest, ObjectsOfTypeResponse
    from guash.requests.sink import OpenSinkRequest, OpenSinkResponse, CloseSinkRequest, CloseSinkResponse
