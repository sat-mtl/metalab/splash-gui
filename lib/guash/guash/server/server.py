from configparser import SectionProxy
from typing import Optional

from guash.controller.controller import SplashController
from guash.notifications.perfs import MasterClock, GetFPS
from guash.notifications.sink import SinkRender
from guash.server.session import GuashRemoteSession
from satlib.tasks import Task, TaskManager
from satnet.server import Server, ServerAdapter, SessionAdapter, RemoteSession


class GuashServer(Server[RemoteSession]):
    def __init__(self, adapter: ServerAdapter, config: Optional[SectionProxy] = None) -> None:
        super().__init__(config=config, adapter=adapter, session_factory=self._guash_client_factory)
        from guash.all_serializable import import_all
        import_all()
        self._controller = SplashController()
        self._task_manager = TaskManager()
        self._tasks = {}  # type Dict[str, Task]

        self._clock_task = None  # type: Task
        self._fps_task = None  # type: Task
        self._sink_task = None  # type: Task
        self._register_timing_notifications()

    def step(self, now: int, dt: int) -> None:
        super().step(now, dt)
        self._task_manager.step(now=now)

    def _guash_client_factory(self, client: SessionAdapter) -> RemoteSession:
        return GuashRemoteSession(
            id=client.id,
            server=self,
        )

    def _register_timing_notifications(self) -> None:
        self._tasks['clock'] = self._task_manager.create_task(
            callback=lambda: self.notify_all(MasterClock(self._controller.get_master_clock())),
            frequency=4
        )

        self._tasks['fps'] = self._task_manager.create_task(
            callback=lambda: self.notify_all(GetFPS(self._controller.get_fps())),
            frequency=2
        )

    @property
    def controller(self) -> SplashController:
        return self._controller

    def notify_sink(self, sink_name: str) -> None:
        self._tasks['sink'] = self._task_manager.create_task(
            callback=lambda: self.notify_all(SinkRender(*self._controller.get_sink_render(sink_name))),
            frequency=5
        )

    def stop_notify(self, task_name: str) -> None:
        task = self._tasks.get(task_name)
        if not task:
            return
        self._task_manager.remove_task(task)
