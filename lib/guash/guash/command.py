from enum import IntEnum, unique
from typing import TYPE_CHECKING

from satnet.command import ClientCommand, ServerCommand

if TYPE_CHECKING:
    # noinspection PyUnresolvedReferences
    from guash.client.session import GuashLocalSession
    # noinspection PyUnresolvedReferences
    from guash.server.session import GuashRemoteSession


@unique
class CommandId(IntEnum):
    """
    Guash Command Ids
    """
    SET_OBJECT_ATTRIBUTE = 0x01
    SET_WORLD_ATTRIBUTE = 0x02
    LOAD_PROJECT = 0x03
    SAVE_PROJECT = 0x04
    LOAD_MEDIA = 0x05
    SUBSCRIBE = 0x06
    UNSUBSCRIBE = 0x07
    PREVIEW_MEDIA = 0x08


class GuashClientCommand(ClientCommand['GuashRemoteSession']):
    """
    Client command, can only be used by clients
    Helper wrapper to save on imports and generic type declarations in concrete guash commands
    """
    pass


class GuashServerCommand(ServerCommand['GuashLocalSession']):
    """
    Server command, can only be used by servers
    Helper wrapper to save on imports and generic type declarations in concrete guash commands
    """
    pass
