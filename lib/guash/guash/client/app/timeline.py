import logging
from typing import Dict
from kivy.config import Config
from kivy.core.window import Window
from kivy.uix.layout import Layout
from kivy.uix.label import Label
from satlib.tasks import Task

logger = logging.getLogger(__name__)


class TimelineLabel(Label):
    pass


class Timeline:
    def __init__(self, update_task: Task, layout: Layout):
        self._media = []  # type List[Dict]
        self._to_add_media = []  # type List[Dict]
        self._to_remove_media = []  # type List[Dict]
        self._update_task = update_task
        self._total_duration = 0
        self._shown_duration = int(Config.get('ui', 'timeline_duration'))
        self._layout = layout
        self._layout.bind(minimum_width=self._layout.setter('width'))  # Needed to scroll the timeline
        self._update = False

    def add_media(self, info: Dict) -> None:
        self._to_add_media.append(info)
        self._update = True

    def remove_media(self, filename: str) -> None:
        for m in [media for media in self._media if media.filename == filename]:
            self._to_remove_media.remove(m)
        self._update = True

    def update(self) -> None:
        if not self._update:
            return
        for m in self._to_add_media:
            filename = m.get('text')
            if filename:
                widget = TimelineLabel(text=filename)
                m['widget'] = widget
                self._media.append(m)
                self._layout.add_widget(widget)
                self._total_duration += m.get('media_duration') or 0.
        for m in self._to_remove_media:
            filename = m.get('text')
            if filename:
                duration = m.get('media_duration') or 0.
                self._total_duration -= duration
        self._to_add_media.clear()
        self._to_remove_media.clear()

        for m in self._media:
            duration = m.get('media_duration')
            if not duration:
                continue
            # For whatever reason, once set the first time, the changes on the width of the widget are not accounted for
            # hint = 1.
            # if self._total_duration > self._shown_duration:
            #     hint = float(duration) / self._shown_duration
            # elif self._total_duration:
            #     hint = float(duration) / self._total_duration
            widget = m.get('widget')
            if not widget:
                continue
            widget.width = duration / self._shown_duration * Window.width

        self._layout.canvas.ask_update()
        self._update = False
