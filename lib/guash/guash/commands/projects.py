import logging
import os
from typing import Optional, TYPE_CHECKING

from guash.command import CommandId, GuashClientCommand
from satnet.command import command

if TYPE_CHECKING:
    from guash.server.session import GuashRemoteSession

logger = logging.getLogger(__name__)


@command(id=CommandId.LOAD_PROJECT)
class LoadProject(GuashClientCommand):
    _fields = ['project_name']

    def __init__(self, project_name: Optional[str] = None) -> None:
        super().__init__()
        self.project_name = project_name

    def handle(self, session: 'GuashRemoteSession') -> None:
        if not self.project_name:
            return

        folder = session.server.controller.get_object_attribute(session.server.controller.get_interpreter_name(), 'projectDirectory')
        if not folder or len(folder) != 1 or not isinstance(folder[0], str):
            logger.error('Invalid value for attribute projectDirectory of object python')
            return

        session.server.controller.load_project(os.path.join(folder[0], self.project_name))


@command(id=CommandId.SAVE_PROJECT)
class SaveProject(GuashClientCommand):
    _fields = ['project_name']

    def __init__(self, project_name: Optional[str] = None) -> None:
        super().__init__()
        self.project_name = project_name

    def handle(self, session: 'GuashRemoteSession') -> None:
        if not self.project_name:
            return

        folder = session.server.controller.get_object_attribute(session.server.controller.get_interpreter_name(), 'projectDirectory')
        if not folder or len(folder) != 1 or not isinstance(folder[0], str):
            logger.error('Invalid value for attribute projectDirectory of object python')
            return

        session.server.controller.save_project(os.path.join(folder[0], self.project_name))
