import logging
from typing import Any, List, Optional, TYPE_CHECKING

from guash.command import CommandId, GuashClientCommand
from satnet.command import command

if TYPE_CHECKING:
    from guash.server.session import GuashRemoteSession

logger = logging.getLogger(__name__)


@command(id=CommandId.SET_OBJECT_ATTRIBUTE)
class SetObjectAttribute(GuashClientCommand):
    _fields = ['object_name', 'attribute', 'value']

    def __init__(self, object_name: Optional[str] = None, attribute: Optional[str] = None,
                 value: Optional[List[Any]] = None) -> None:
        super().__init__()
        self.object_name = object_name
        self.attribute = attribute
        self.value = value

    def handle(self, session: 'GuashRemoteSession') -> None:
        session.server.controller.set_object_attribute(self._object_name, self._attribute, self._value)


@command(id=CommandId.SET_WORLD_ATTRIBUTE)
class SetWorldAttribute(GuashClientCommand):
    _fields = ['attribute', 'value']

    def __init__(self, attribute: Optional[str] = None, value: Optional[List[Any]] = None) -> None:
        super().__init__()
        self.attribute = attribute
        self.value = value

    def handle(self, session: 'GuashRemoteSession') -> None:
        session.server.controller.set_world_attribute(self.attribute, self.value)
