from enum import IntEnum, unique
from typing import TYPE_CHECKING

from satnet.action import Action

if TYPE_CHECKING:
    # noinspection PyUnresolvedReferences
    from guash.server.session import GuashRemoteSession


@unique
class ActionId(IntEnum):
    """
    Guash Action Ids
    """
    TRANSACTION = 0x01


class GuashAction(Action['GuashRemoteSession']):
    """ Helper wrapper to save on imports and generic type declarations in concrete guash actions """
    def apply(self, session: 'GuashRemoteSession') -> None:
        pass

    def revert(self, session: 'GuashRemoteSession') -> None:
        pass
