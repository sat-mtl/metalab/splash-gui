import os
import sys
import threading
import argparse

# Add paths to local libs
path = os.path.dirname(os.path.realpath(__file__))
sys.path.insert(0, os.path.realpath(os.path.join(path, "../extlib/satlib")))
sys.path.insert(0, os.path.realpath(os.path.join(path, "../extlib/satnet")))
sys.path.insert(0, os.path.realpath(os.path.join(path, "../lib/guash")))

from typing import Optional
from satnet.adapters.websockets.server import WSServer
from satnet.message import MessageParser
from guash.server.server import GuashServer
import time

# Setup logger
import logging
from logging import StreamHandler
from satlib.logging import SATFormatter
import splash

handler = StreamHandler(stream=sys.stdout)
handler.setFormatter(SATFormatter())
logging.basicConfig(level=logging.DEBUG, handlers=[handler])
logging.getLogger('satnet.adapters').setLevel(logging.INFO)
logging.getLogger('websockets.protocol').setLevel(logging.INFO)

mediaDirectory = os.path.join(os.path.expanduser('~'), 'src/splash/media_dir')
activeMediaDirectory = os.path.join(os.path.expanduser('~'), 'src/splash/active_media_dir')
projectDirectory = os.path.join(os.path.expanduser('~'), 'src/splash/project_dir')

server = None  # type: Optional[GuashServer]
port = 8765  # type: int
guash_thread = None  # type: Optional[threading.Thread]


def parse_arguments():
    global mediaDirectory, activeMediaDirectory, projectDirectory

    if not len(sys.argv):
        return

    parser = argparse.ArgumentParser()
    parser.add_argument("--mediadir", type=str, help="set media directory", default="")
    parser.add_argument("--activemediadir", type=str, help="set active media directory", default="")
    parser.add_argument("--projectdir", type=str, help="set project directory", default="")
    args = parser.parse_args()

    if args.mediadir != '':
        mediaDirectory = args.mediadir
    if args.activemediadir != '':
        activeMediaDirectory = args.activemediadir
    if args.projectdir != '':
        projectDirectory = args.projectdir
        

def splash_init():
    parse_arguments()

    print("")
    print("============= Guash configuration =============")
    print("Media directory          ", mediaDirectory)
    print("Active media directory   ", activeMediaDirectory)
    print("Project directory        ", projectDirectory)
    print("")

    set_splash_configuration()

    # Setup networking
    global server
    global port
    server = GuashServer(adapter=WSServer(message_parser=MessageParser()))
    if server:
        server.start(port=port)

    def run():
        last = time.time()
        while True:
            now = time.time()
            dt = now - last
            last = now

            # Process incoming messages in the main thread
            server.step(now, dt)
            time.sleep(0.001)

    # Run the network server
    global guash_thread
    guash_thread = threading.Thread(target=run)
    if guash_thread:
        guash_thread.start()


def splash_loop():
    pass


def splash_stop():
    pass


def set_splash_configuration():
    splash.add_custom_attribute('mediaDirectory')
    splash.add_custom_attribute('activeMediaDirectory')
    splash.add_custom_attribute('projectDirectory')
