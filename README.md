# Guash: a GUI for Splash

Guash is a native GUI used to control the [Splash](https://gitlab.com/sat-metalab/splash) video mapper.
Splash already has a built-in interface, but it's rather crude and cannot be used from another computer.
The idea of Guash is to run either on a computer or a mobile phone/tablet (Android only 
probably) on the same network as Splash.

### Underlying network management
The network messaging in Guash relies on the [satnet](https://gitlab.com/sat-metalab/py-satnet) network library and 
its [websockets](https://websockets.readthedocs.io/en/stable/) adapter has been written for this purpose.

### How to install
Get the latest Guash source code and its submodules:

`git clone https://gitlab.com/sat-metalab/splash-gui`

`git sumodule update --init --recursive`


Install necessary software dependencies (only needed on the client side):
 
`sudo apt install xsel xclip`

`sudo pip3 install cython==0.26 pygame kivy websockets msgpack-python`


### Application framework
For the UI part, the python framework [Kivy](https://kivy.org) has been chosen since it is rather easy to use and 
flexible and it also allows a pretty straightforward integration in Android applications.

### How to run Guash
#### Server side
What we call server side is actually where [Splash](https://gitlab.com/sat-metalab/splash) is run. [Splash](https://gitlab.com/sat-metalab/splash) has a nice feature that is an embedded Python interpreter allowing to run 
Python code directly from it alongside [Python bindings](https://gitlab.com/sat-metalab/splash/wikis/Technical_Information#python_scripting) allowing a vast range of actions on the running instance.

Currently when you want Guash to be run, you need to configure your scene so it runs the guash\_server.py python file.
 It might be more automatized later.
 
Of course splash must be compiled first on the server, see the documentation for that.

When running splash, you can modify global attributes that configure the asset directories, there are currently two 
such attributes: 
* currentMediaDirectory: the drop-in folder for media assets 
* activeMediaDirectory: the folder where activated media assets will be copied
* projectDirectory: the folder where the project files will be fetched from and saved. 


#### Client side
The client side is the actual application that must connect through websocket to the server side part of Guash run by
 [Splash](https://gitlab.com/sat-metalab/splash) by providing the correct IP address and the port used (by default 
8765, hardcoded currently). The IP is configurable in the settings panel accessible from the application.

For the moment, to run it, go in the splash-gui folder and run:

`python3 client` 
