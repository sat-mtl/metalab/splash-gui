"""
Guash Client Application

This is the main file, it sets up the environment before launching the application
"""

import os
import sys

# Add paths to local libs
path = os.path.dirname(os.path.realpath(__file__))
sys.path.insert(0, os.path.realpath(os.path.join(path, "../extlib/satlib")))
sys.path.insert(0, os.path.realpath(os.path.join(path, "../extlib/satnet")))
sys.path.insert(0, os.path.realpath(os.path.join(path, "../extlib/KivyDnD")))
sys.path.insert(0, os.path.realpath(os.path.join(path, "../lib/guash")))

# Command line parsing
from argparse import ArgumentParser
if not hasattr(sys, 'argv'):
    sys.argv = ['']
parser = ArgumentParser(description="Guash Client")
args = parser.parse_args()

# Setup logger
import logging
from logging import StreamHandler
from satlib.logging import SATFormatter

handler = StreamHandler(stream=sys.stdout)
handler.setFormatter(SATFormatter())
logging.basicConfig(level=logging.DEBUG, handlers=[handler])
logging.getLogger('satnet.adapters').setLevel(logging.INFO)
logging.getLogger('websockets.protocol').setLevel(logging.INFO)


def main() -> None:
    """
    Application's main method
    :return:
    """
    from ClientApplication import ClientApplication
    try:
        ClientApplication().run()
    except KeyboardInterrupt:
        sys.exit(0)


# Allow launching from the package directly
if __name__ == "__main__":
    main()
